CREATE TABLE game (
    game_id SERIAL PRIMARY_KEY,
    parent_id integer NOT NULL,
    black_player_id integer REFERENCES user(user_id),
    white_player_id integer REFERENCES user(user_id),
    start_dt timestamp,
    duration_sec integer,
    winner char(5),
    has_tournament boolean NOT NULL
);

CREATE TABLE tournament (
    tournament_id SERIAL PRIMARY_KEY,
    rules_id integer REFERENCES rules(rules_id),
    start_dttm timestamp NOT NULL,
    end_dttm timestamp NOT NULL,
    type varchar(50) NOT NULL,
    min_rating integer,
    max_rating integer
);

CREATE TABLE rules (
    rules_id SERIAL PRIMARY KEY,
    board_size integer,
    time_limit_sec integer 
);

CREATE TABLE user (
    user_id SERIAL PRIMARY KEY,
    user_nm varchar(50),
    country varchar(50),
    registration_dt date NOT NULL,
    rating INTEGER,
    valid_from_dttm timestamp NOT NULL,
    valid_to_dttm timestamp NOT NULL
);

CREATE TABLE puzzle (
    puzzle_id SERIAL PRIMARY KEY,
    author_id integer REFERENCES user(user_id),
    puzzle_nm varchar(50) NOT NULL UNIQUE,
    source varchar(50),
    difficulty integer,
    date_added date NOT NULL
);

CREATE TABLE attempt (
    user_id integer REFERENCES user(user_id),
    puzzle_id integer REFERENCES puzzle(puzzle_id),
    attempt_dttm timestamp NOT NULL,
    success boolean NOT NULL
);

CREATE TABLE tournament_results (
    tournament_id integer REFERENCES tournament(tournament_id),
    user_id integer REFERENCES user(user_id),
    place_num integer
);


INSERT INTO rules (board_size, time_limit_sec) VALUES (19, 3600);

SELECT * FROM rules;

